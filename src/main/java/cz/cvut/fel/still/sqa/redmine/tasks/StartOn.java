package cz.cvut.fel.still.sqa.redmine.tasks;

import cz.cvut.fel.still.sqa.redmine.pageobjects.ApplicationHomePage;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.HoverOverBy;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Vaclav Rechtberger
 */
public class StartOn implements Task {
    ApplicationHomePage applicationHomePage;
    @Override
    @Step("{0} starts at home page without any logged user")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(applicationHomePage),
                HoverOverBy.over(By.xpath("//a[@class='login']"))
        );
    }

    public static StartOn aLoggedoutPage() {
        return instrumented(StartOn.class);
    }
}
