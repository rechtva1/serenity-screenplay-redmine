package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Vaclav Rechtberger
 */
public class MainMenu extends PageObject {
    public static Target NEW_ISSUE_LINK = Target.the("New issue task.").located(By.cssSelector("*.new-issue"));
}
