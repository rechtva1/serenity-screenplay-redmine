package cz.cvut.fel.still.sqa.redmine.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

/**
 * @author Vaclav Rechtberger
 */
public class RegistrationForm extends PageObject {
    public static Target LOGIN_FIELD = Target.the("Login field").located(By.id("user_login"));
    public static Target PASSWORD_FIELD = Target.the("Password field").located(By.id("user_password"));
    public static Target PASSWORD_FIELD_CONFIRMATION = Target.the("Password confirmation field").located(By.id("user_password_confirmation"));
    public static Target FIRSTNAME_FIELD = Target.the("Firstname field").located(By.id("user_firstname"));
    public static Target LASTNAME_FIELD = Target.the("Lastname field").located(By.id("user_lastname"));
    public static Target MAIL_FIELD = Target.the("Email field").located(By.id("user_mail"));
    public static Target SUBMIT = Target.the("Submit registartion form").locatedBy("//input[@type='submit']");
}
