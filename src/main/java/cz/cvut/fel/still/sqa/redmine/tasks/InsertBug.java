package cz.cvut.fel.still.sqa.redmine.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

import static cz.cvut.fel.still.sqa.redmine.pageobjects.MainMenu.NEW_ISSUE_LINK;
import static cz.cvut.fel.still.sqa.redmine.pageobjects.NewIssueForm.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Vaclav Rechtberger
 */
public class InsertBug implements Task {
    private String name;
    private String description;

    public InsertBug(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Step("{0} inserts bug named: '#name' with description: '#description'")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(NEW_ISSUE_LINK),
                Enter.theValue(name).into(NEW_ISSUE_SUBJECT),
                Enter.theValue(description).into(NEW_ISSUE_DESCRIPTION),
                Click.on(SUBMIT)
        );
    }

    public static InsertBug describedWith(String name, String description) {
        return instrumented(InsertBug.class, name, description);
    }
}